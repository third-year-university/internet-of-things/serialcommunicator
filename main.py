import time

from communicator import Communicator

com = Communicator('/dev/ttyUSB0', 9600)

time.sleep(2)
com.request_data_once(14)
com.request_data_once(3)
com.repeat_last(2)
com.request_all_digital()
com.set_all_digitals([2, 3])