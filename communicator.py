import time

import serial


class Communicator:
    con = None

    def __init__(self, port, rate) -> None:
        super().__init__()
        self.con = serial.Serial(port, rate, timeout=1)

    @staticmethod
    def _count_hash(data):
        h = 0
        for d in data:
            highorder = h & 0xF8000000
            h = (h << 5) & 0xFFFFFFFF
            h = h ^ (highorder >> 27)
            h = h ^ d
        # print(f"Hash {h & 0xFF}")
        return h & 0xFF

    def _send_data(self, data, answer_length):
        arr = bytearray()
        for d in data:
            arr.append(d)
        arr.append(self._count_hash(data))
        #print(f"Hash: {self._count_hash(data)}")
        local_hash = 0
        remote_hash = -1
        while remote_hash != local_hash:
            self.con.flushInput()
            self.con.flushOutput()
            self.con.write(arr)
            while self.con.in_waiting < answer_length:
                time.sleep(0.1)
                print("Ждём...")
            data = self.con.read(answer_length)
            remote_hash = data[-1]
            local_hash = self._count_hash(data[:-1])
            if local_hash != remote_hash:
                print("Хэши не сошлись, повторяем запрос")
        return data

    def request_data_once(self, pin):
        answer_length = 2
        if pin > 13:
            answer_length = 3
        data = self._send_data([ord('R'), pin], answer_length)
        if pin > 13:
            print(f"Состояние пина {pin}: {int(data[0]) * 256 + int(data[1])}")
        else:
            print(f"Состояние пина {pin}: {int(data[0])}")
        print("======================\n")

    def request_data_stream(self, pin):
        answer_length = 2
        data = self._send_data([ord('S'), pin], answer_length)
        print(f"Поток с пина {pin} организован")
        print("======================\n")

    def repeat_last(self, answer_length):
        data = self._send_data([ord('L')], answer_length)
        if answer_length == 2:
            print(f"Повтор данных: {int(data[0])}")
        elif answer_length == 3:
            print(f"Повтор данных {int(data[0]) * 256 + int(data[1])}")
        print("======================\n")

    def request_all_digital(self):
        answer_length = 3
        data = self._send_data([ord('d')], answer_length)
        d = int(data[0]) * 256 + int(data[1])
        print("Состояние портов: ")
        for i in range(2, 14):
            print(f"Порт {i}: {'HIGH' if (d >> i) & 1 == 1 else 'LOW'}")
        print("======================\n")

    def set_all_digitals(self, pins):
        print("Задаём значения пинов")
        answer_length = 2
        a = 0
        for pin in pins:
            a |= 1 << pin
        data = self._send_data([ord('D'), (a >> 8) & 0xFF, a & 0xFF], answer_length)
        print("Готово")
        print("======================\n")
