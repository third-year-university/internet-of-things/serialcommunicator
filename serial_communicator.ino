byte pin;
bool do_stream = false;
byte data[] = {0, 0, 0, 0, 0, 0, 0, 0};
//byte received[] = {0, 0, 0, 0, 0, 0, 0, 0};
int length = 0;

void readPin(int pin){
  pinMode(pin, INPUT);
  if (pin <= 13){
    data[0] = digitalRead(pin);
    //Serial.write(pin);
    send_data(1);
  }
  else{
    int value = analogRead(pin);
    data[0] = value / 256;
    data[1] = value % 256;
    send_data(2);
  }
}

byte count_hash(byte* d, int size){
  long h = 0;
  for (int i = 0; i < size; i++){
    long highorder = h & 0xF8000000;
    h = (h << 5) & 0xFFFFFFFF;
    h = h ^ (highorder >> 27);
    h = h ^ d[i];
  }
  return byte(h&0xFF);
}

void send_data(int size){
  length = size;
  data[size] = count_hash(data, size);
  Serial.write(data, size + 1);
}

bool check_data(byte* received, int length, byte h){
  if (count_hash(received, length) == h){
    return true;
  }
  else{
    Serial.write(255);
    Serial.write(255);
    Serial.write(255);
    return false;
  }
}

void setup() {
  Serial.begin(9600);
  Serial.write(100);
}

void loop() {
  while (Serial.available() > 0) {
    do_stream = false;
    byte cmd = Serial.read();
    if (cmd == 'R') {
      while (Serial.available() < 2){
        delay(50);
      }
      pin = Serial.read();
      int h = Serial.read();
      byte received[] = {cmd, pin};
      if (!check_data(received, 2, h)){
        break;
      }
      readPin(pin);
    } 
    else if (cmd == 'S') {
      while (Serial.available() < 2){
        delay(50);
      }
      pin = Serial.read();
      int h = Serial.read();
      byte received[] = {cmd, pin};
      if (!check_data(received, 2, h)){
        break;
      }
      do_stream = true;
      data[0] = 222;
      send_data(1);
    }
    else if (cmd == 'L'){
      while (Serial.available() < 1){
        delay(50);
      }
      int h = Serial.read();
      byte received[] = {cmd};
      if (!check_data(received, 1, h)){
        break;
      }
      send_data(length);
    }
    else if (cmd == 'd'){
      while (Serial.available() < 1){
        delay(50);
      }
      int h = Serial.read();
      byte received[] = {cmd};
      if (!check_data(received, 1, h)){
        break;
      }
      uint16_t ans = 0;
      for (int i = 2; i <= 13; i++){
        ans |= digitalRead(i) << i;
      }
      data[0] = ans / 256;
      data[1] = ans % 256;
      send_data(2);
    }
    else if (cmd = 'D'){
      while (Serial.available() < 3){
        delay(50);
      }
      byte b1 = Serial.read();
      byte b2 = Serial.read();
      int h = Serial.read();
      byte received[] = {cmd, b1, b2};
      if (!check_data(received, 3, h)){
        break;
      }
      uint16_t a = (b1 << 8) | b2;
      for (int i = 2; i <= 13; i++){
        pinMode(i, OUTPUT);
        digitalWrite(i, bool((a >> i) & 1));
      }
      data[0] = 112;
      send_data(1);
    }
  }
  if (do_stream){
    readPin(pin);
  }
  delay(50);
}
